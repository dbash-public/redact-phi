const strategyEngine = require('./strategy-engine')

module.exports = (config) => {
    return strategyEngine(config);
}

// check if parent value has been converted


function tabular(config) {
    return (data) => strategyEngine(data);
}

function jason(config) {
    return data;
}

function redactOTron(columnConfig, data) {
    return data;
}