const fs = require('fs').promises;
const readline = require('readline');
const homedir = require('os').homedir();
const redactPath = `${homedir}/.redact/`;
const configPath = `${redactPath}telemetry.json`;
const axios = require('axios');
let metrics = [];
async function init() {
    for (let element of process.argv) {
        if (element === '-y') {
            return gatherMetric;
        }
    }
    let exists = false, config = false;
    try {
        exists = await fs.stat(configPath);
        config = require(configPath);
        return getFunction(config);

    } catch (err) { }

    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    return new Promise((resolve) => {
        config = {
            optIn: false
        };
        const question = `Help improve redact-phi!\nYou can help us improve with crash reports and usage metrics (no PHI / PII will be sent).\nY/n:`
        rl.question(question, async (answer) => {
            if (answer === '' || answer.toLowerCase() === 'y' || answer.toLowerCase() === 'yes') {
                config.optIn = true;
            } else {
                config.optIn = false;
            }

            // This handles if the .redact folder exists
            try {
                await fs.mkdir(redactPath);
            } catch (err) {
                if (!err.code.includes('EXIST')) {
                    throw err;
                }
            }
            try {
                await fs.writeFile(configPath, JSON.stringify(config))
                rl.close();
                resolve(getFunction(config));

            } catch (err) {
                console.log('Unable to write to ~/redact to save settings');
                resolve(getFunction(config));
            }
        });
    });
}

function getFunction(config) {
    if (config.optIn) {
        return gatherMetric;
    }

    return noOp;
}

function noOp() {
    return true;
}

function gatherMetric(runId, meta) {
    metrics.push({
        runId,
        meta,
        datetime: new Date()
    });

    return true;
}

async function sendMetrics() {
    if (metrics.length === 0) {
        return true;
    }
    const metricsClone = JSON.parse(JSON.stringify(metrics));
    metrics = [];
    await axios.post(`https://telemetry.redact-phi.workers.dev/`, {
        datetime: new Date(),
        metrics: metricsClone
    }).catch(() => { });
}

module.exports.init = init;
module.exports.sendMetrics = sendMetrics;