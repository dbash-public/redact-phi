#!/usr/bin/env node
const crypto = require('crypto');
const telemetry = require('../telemetry');
const { Command } = require('commander');
const program = new Command();
const path = require('path')
const fs = require('fs');
const csv = require('csv');
const process = require('process');
program.version('1.0.0');
program.showHelpAfterError();
const runId = crypto.randomBytes(16).toString('hex');
let metric;

async function main() {
    metric = await telemetry.init();
    metric(runId, {
        start: true
    });

    program
        .option('--delimiter <delimiter>', 'sets delimiter type', ',')
        .option('-y', 'Skips all prompts')
        .option('--strategy <override>', 'Provide the location of your redaction specification')
        .argument('<infile>', 'File to redact')
        .argument('[outfile]', 'Output file, defaults to {name_redacted.csv}');

    program.parse(process.argv)
        .action(async (infile, outfile) => {
            metric(runId, {
                infile: typeof infile === 'string',
                outfile: typeof outfile === 'string'
            })
            if (infile === 'help') {
                metric(runId, {
                    'help': true
                });
                console.log('Please run again with --help flag');
                await telemetry.sendMetrics();
                return process.exit(0);
            }
            const options = program.opts();
            const override = options.override;
            const fileExtension = path.extname(infile);
            metric(runId, {
                fileExtension, override
            });
            const absolutePath = path.join(process.cwd(), infile);
            if (!outfile) {
                outfile = absolutePath.replace(fileExtension, `_redacted${fileExtension}`);
            }
            const engine = require('../strategy-engine')(await getConfiguration(absolutePath, override));
            if (fileExtension === '.csv') {
                await handleCsv(engine, absolutePath, outfile);
            } else {
                console.error(`File type ${fileExtension} is currently unsupported, xslx and JSON are coming soon!`);
                await telemetry.sendMetrics();
                process.exit(-1);
            }
        })
    program.parse();
}

main();

async function handleCsv(engine, absolutePath, outfile) {
    console.log('Starting Redaction');
    let count = 0;
    const options = program.opts();
    const stringifier = csv.stringify({
        delimiter: options.delimiter
    });
    metric(runId, {
        delimiter: options.delimiter
    });
    try {
        fs.statSync(absolutePath)
    } catch (err) {
        console.error(`Input file '${absolutePath}' does not exist or cannot be read`);
        metric(runId, {
            name: err.name,
            code: err.code
        })
        await telemetry.sendMetrics();
        process.exit(-1);
    }
    const parser = fs.createReadStream(absolutePath).pipe(csv.parse());
    const outStream = fs.createWriteStream(outfile);
    stringifier.on('readable', () => {
        let row;
        while (row = stringifier.read()) {
            outStream.write(row);
        }
    });

    parser.on('readable', () => {
        while (data = parser.read()) {
            count++;
            stringifier.write(engine.process(data));
        }
    });

    parser.on('end', () => {
        metric(runId, {
            end: true,
            count
        });
        telemetry.sendMetrics();

        console.log(`Finished Redacting, ${count} records processed`);
        console.log(`Created File: ${outfile}`);
    })
}

async function getConfiguration(absolutePath, override) {
    const fileExtension = path.extname(absolutePath);
    let configFilePath = override ? path.join(process.cwd(), override) : absolutePath.replace(fileExtension, '.json');
    let config = false;
    try {
        await fs.promises.stat(configFilePath);
        config = configFilePath;
    } catch (err) {
        metric(runId, {
            error: err,
            code: err.code
        });
    }
    if (config === false) {
        await telemetry.sendMetrics();
        throw new Error('No suitable configuration file found');
    }

    let configuration = require(config);
    try {
        configuration.strategies = require(absolutePath.replace(fileExtension, '.js'));
        await fs.promises.stat(absolutePath.replace(fileExtension, '.js'));
        metric(runId, {
            defaultStrategy: false
        });
    } catch (err) {
        metric(runId, {
            defaultStrategy: true
        });
        configuration.strategies = {};
    }
    telemetry.sendMetrics();
    return configuration;
}

process.on('beforeExit', async () => {
    return await telemetry.sendMetrics();
});

process.on('SIGTERM', async () => {
    return await telemetry.sendMetrics();
});

process.on('uncaughtException', async (err, origin) => {
    metric(runId, {
        uncaughtException: true,
        errorName: err.name,
        errorMessage: err.message,
        origin
    })
    await telemetry.sendMetrics();
});