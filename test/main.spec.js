const fs = require('fs');
const csv = require('csv');

const parser = fs.createReadStream('./data/example.csv').pipe(csv.parse());

let configOld = {
    "fileType": "csv",
    "columns": [
        {
            "redactWith": "{{name.lastName}}, {{name.firstName}} {{name.suffix}}",
            "columnNum": 0,
            "columnKey": "",
            "tracked": true,
        },
        {
            "redactWith": "genericProductId",
            "columnNum": 1,
            "columnKey": "",
            "tracked": false,
        },
        {
            "redactWith": "999",
            "columnNum": 2,
            "columnKey": "",
            "tracked": false,
        },
        {
            "redactWith": "finance.accountName",
            "columnNum": "",
            "columnKey": "first name",
            "tracked": true,
        }
    ]
};

const config = require('./data/example.json')
const engine = require('../strategy-engine')(config);

parser.on('readable', function () {
    while (data = parser.read()) {
        console.log(engine.process(data));
    }
});