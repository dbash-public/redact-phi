const { exec } = require('child_process');
const expect = require('chai').expect;
const base = 'node ./bin/redact.js';
describe('Command-line interface', async () => {
    it('should be able to call the base help function', async () => {

        let result = await execPromise('--help');
        expect(result.includes('redact [options]')).to.be.true;
    })

    it('should be able to provide only a .csv', async () => {
        let result = await execPromise('./example/example.csv');
        expect(result.includes('records processed')).to.be.true;
    })

})

async function execPromise(command) {
    return new Promise((resolve, reject) => {
        exec(`${base} ${command} -y`, { cwd: process.cwd() }, (error, stdout, stderr) => {
            if (error) {
                reject({
                    error,
                    stderr,
                    stdout
                })
                return;
            }
            resolve(stdout);
        });
    })
}
